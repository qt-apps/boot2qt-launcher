// Copyright (C) 2023 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

pragma Singleton

import QtQuick

QtObject {
    readonly property real margin: 0.03
    readonly property string appFont: "TitilliumWeb"
    readonly property color backgroundColor: "#00414a"
    readonly property color demolistBackgroundColor: "#002e34"
    readonly property color borderColor: "#9d9faa"
    readonly property color buttonGreenColor: "#2cde85"
    readonly property color buttonGrayColor: "#9d9faa"
    readonly property color buttonActiveColor: "#216729"
    readonly property color loaderBackgroundColor: "#0a8550"
    readonly property color loaderForegroundColor: "#2cde85"
    readonly property color scrollBarColor: "#2cde85"
    readonly property color neon: "#2cde85"
    readonly property color pine: "#00414a"
}
